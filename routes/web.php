<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain('http://search.loc')->group(function(){
	Route::get('/', 'StateController@index');
	Route::get('states/geo', 'StateController@geo');
	Route::post('states', 'StateController@store');
	Route::get('transponders/{satelite}', 'TpController@index');
	Route::get('channels', 'ChannelController@index');
	Route::get('models/{id}', 'StateController@filter');
	Route::get('/states/{state}/comments', 'CommentController@index');
	Route::post('states/{state}/comments', 'CommentController@store');
	Route::post('authenticate', 'AuthJwtController@auth');
	Route::post('users', 'UserController@store');
	Route::middleware('jwt.auth')->group(function(){
		Route::get('models', 'StateController@model');
		Route::post('users/models', 'UserController@notification');
		Route::put('users/edit', 'UserController@update');
		Route::post('suscribe/{state}/state', 'CommentController@suscribe');
		Route::get('users/suscriptions', 'UserController@suscriptions');
		Route::delete('users/suscriptions/{id}', 'UserController@delete');
	});
});