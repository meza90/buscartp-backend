<?php

namespace App\Http\Controllers;

use App\Satelite;
use App\Tp;
use Illuminate\Http\Request;

class TpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $satelite = Satelite::name($request->satelite)
                        ->firstOrFail();

        $transponders = Tp::where('satelite_id', $satelite->id)
                        ->get();

        return response()->json(array(
            'transponders' => $transponders->chunk(3),
            'count' => $transponders->count()
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tp  $tp
     * @return \Illuminate\Http\Response
     */
    public function show(Tp $tp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tp  $tp
     * @return \Illuminate\Http\Response
     */
    public function edit(Tp $tp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tp  $tp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tp $tp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tp  $tp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tp $tp)
    {
        //
    }
}
