<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Mark;
use App\Type;
use App\State;
use App\TypeUser;
use Illuminate\Http\Request;
use App\Http\Requests\StateRequest;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $states = State::relation()
                    ->orderByDesc('created_at')
                    ->sats()
                    ->getOf('state', $request->state)
                    ->getOf('method', $request->method)
                    ->when($request->model_id, function($q){
                        $id = qString('model_id');
                        $q->whereIn('model_id', $id);
                    })
                    ->when($request->mark_id, function($q) use($request){
                        $q->whereHas('type', function($q) use($request){
                            $q->where('mark_id', $request->mark_id);
                        });
                    })
                    ->withCount('comments')
                    ->paginate(20);

        return response()->json(array(
                'states' => $states
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StateRequest $request)
    {
        if(count($request->satelite_id) > 3)
            return response()->json(array('message' => 'Puedes escoger máximo 3 satélites'), 423);

        $state = State::create($request->only('model_id', 'state', 'method', 'comment', 'update') +
                    [
                        'country_id' => jwtCheck() ? NULL : $request->country_id,
                        'ip' => $request->ip(),
                        'user_id' => jwtCheck() ? JWTAuth::parseToken()->authenticate()->id : NULL
                    ])->satelites()->attach($request->satelite_id);

        return response()->json([], 200);
    }

    /**
     * Filtrar los modelos por marca.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filter($id)
    {
        $models = Type::orderBy('name')
                    ->where('mark_id', $id)
                    ->get();

        return response()->json(array(
                'models' => $models
        ));
    }

    /**
     * Obtener país automaticamente.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function geo()
    {
        $countries = countries();

        $ip = ip(request()->ip());

        if($ip['status'] == 'success'){

            $detected = $ip['country'];

            return $country = $countries->filter(function($value, $key) use($detected){
                return $value['name'] === $detected;
            });

        } else {
            return;
        }
    }

    public function model(Request $request)
    {
        $selected = TypeUser::where('user_id', $request->user()->id)
                            ->pluck('type_id');

        $marks = \Cache::rememberForever('marks', function () {
            return Mark::with('types')
                        ->orderBy('name')
                        ->get();
        });

        $data = [];

        foreach ($marks as $mark) {
            foreach ($mark->types as $model) {
                $data[] = [
                    'id' => $model->id,
                    'name' => "{$mark->name} {$model->name}"
                ];
            }
        }

        return response()->json(array(
            'models' => $data,
            'selected' => $selected
        ));
    }
}
