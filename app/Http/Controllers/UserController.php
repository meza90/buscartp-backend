<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequestCreate;
use App\Http\Requests\UserRequestUpdate;
use App\StateUser;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;

class UserController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequestCreate $request)
    {
        $user = User::create($request->all());

        $token = JWTAuth::attempt(array('email' => $user->email, 'password' => $request->password));

        return response()->json(compact('token', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequestUpdate $request)
    {
        $user = $request->user();

        $user->update($request->all());

        return response()->json(compact('user'));
    }

    /**
     * Notificaciones segun modelo
     * @param Request $request
     * @return type
     */
    public function notification(Request $request)
    {
        $request->user()->subscribed()->sync($request->type_id);

        return response()->json([], 200);
    }

    /**
     * Todos los estados a los que el usuario está suscritp
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function suscriptions(Request $request)
    {
        $suscriptions = $request->user()
                    ->subscribed()
                    ->with('state', 'state.type:id,mark_id,name', 'state.type.mark:id,name')
                    ->orderByDesc('created_at')
                    ->get();

        $states = $request->user()
                    ->states()
                    ->with('type.mark')
                    ->get();

        $comments = $request->user()
                    ->comments()
                    ->with('state.type.mark')
                    ->get();

        return response()->json(compact('suscriptions', 'states', 'comments'));
    }

    /**
     * Eliminado un estado suscrito
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        StateUser::where('id', $request->id)->firstOrFail()->delete();

        return response()->json([], 200);
    }
}
