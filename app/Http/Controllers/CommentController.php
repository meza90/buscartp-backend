<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\State;
use App\Comment;
use App\StateUser;
use App\Mail\NotifyComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\CommentRequest;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(State $state)
    {
        $comments = Comment::select('body', 'created_at')
                        ->where('state_id', $state->id)
                        ->orderByDesc('created_at')
                        ->get();

        $exists = $this->exists($state->id);

        return response()->json(array(
            'comments' => $comments,
            'state' => $state->load('type', 'type.mark'),
            'exists' => $exists
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(State $state, CommentRequest $request)
    {
        $comment = Comment::create([
            'body' => $request->body,
            'state_id' => $state->id,
            'ip' => $request->ip(),
            'user_id' => jwtCheck() ? JWTAuth::parseToken()->authenticate()->id : NULL
        ]);

        return response()->json(array(
            'comment' => $comment->body,
            'created_human_at' => $comment->created_human_at
        ));
    }

    /**
     * Suscribirse a un estado
     * @param Comment $comment
     * @return type
     */
    public function suscribe(State $state)
    {
        $state->suscriptions()->create(array('user_id' => request()->user()->id));

        return response()->json([], 200);
    }

    /**
     * Verificar si se está suscrito a un estado
     * @param type $id
     * @return type
     */
    public function exists($state)
    {
        $exists = false;

        if(jwtCheck())
        {
            $exists = StateUser::where('user_id', JWTAuth::parseToken()->authenticate()->id)
                        ->where('state_id', $state)
                        ->count();
        }

        return $exists;
    }
}
