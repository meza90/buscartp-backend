<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Tp;
use Illuminate\Http\Request;

class ChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $channels = Tp::with('channels')
                ->withCount('channels')
                ->when($request->transponder, function($q) use($request){
                    $satelites = qString('transponder');
                    $q->whereIn('id', $satelites);
                })
                ->getSat($request->satelite ?: 2)
                ->orderAsc()
                ->get();

        $transponders = Tp::orderAsc()
                    ->getSat($request->satelite ?: 2)
                    ->get();

        return response()->json(array(
            'channels' => $channels,
            'transponders' => $transponders
        ));
    }
}
