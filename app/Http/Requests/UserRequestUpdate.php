<?php

namespace App\Http\Requests;

use JWTAuth;
use Illuminate\Foundation\Http\FormRequest;

class UserRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'unique:users,email,'.JWTAuth::authenticate()->id,
            'name' => 'required|max:15'
        ];

        if($this->password) return array_add($rules, 'password', 'min:6');

        return $rules;
    }
}
