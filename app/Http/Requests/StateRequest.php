<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'state' => 'required|in:ON,OFF,FREEZ',
            'model_id' => 'required|exists:models,id',
            'mark_id' => 'required|exists:marks,id',
            'update' => 'required|max:15',
            'method' => 'required|in:IKS,SKS,IPTV,SKS/IKS,SOLO IKS PRIVADO',
        ];

        if($this->comment) return array_add($rules, 'comment', 'min:5');

        return $rules;
    }
}
