<?php

use JWTAuth as Auth;

if (! function_exists('qString')) {
    /**
     * Obtener query string con el mismo nombre.
     *
     * @param  string  $key
     */
    function qString($key)
    {
        $query  = explode('&', request()->server->get('QUERY_STRING'));
        $params = array();

        foreach( $query as $param )
        {
          list($name, $value) = explode('=', $param, 2);
          $params[urldecode($name)][] = urldecode($value);
        }

        return $params[$key];
    }
}

if (! function_exists('ip')) {
    /**
     * Enviar IP a http://ip-api.com/php
     *
     * @param  string  $key
     */
    function ip($ip)
    {
        return @unserialize(file_get_contents("http://ip-api.com/php/{$ip}"));
    }
}

if (! function_exists('countries')) {
    /**
     * Retornar paises de Súdamerica
     */
    function countries()
    {
        return collect([
            ['id' => 1, 'name' => "Chile"], ['id' => 2, 'name' => "Argentina"],
            ['id' => 3, 'name' => "Ecuador"], ['id' => 4, 'name' => "Colombia"],
            ['id' => 5, 'name' => "Perú"], ['id' => 6, 'name' => "Bolivia"],
            ['id' => 7, 'name' => "Venezuela"], ['id' => 8, 'name' => "Uruguay"],
            ['id' => 9, 'name' => "Paraguay"], ['id' => 10, 'name' => "Brasil"],
            ['id' => 11, 'name' => "México"], ['id' => 12, 'name' => "Panamá"],
            ['id' => 13, 'name' => "Guatemala"], ['id' => 14, 'name' => "El Salvador"],
            ['id' => 15, 'name' => "Nicaragua"], ['id' => 16, 'name' => "Estados Unidos"]
        ]);
    }
}

if (! function_exists('links')) {
    /**
     * Construir links a traves del string
     */
    function links($comment) {

        $comment = strip_tags($comment, '<a>');

        return preg_replace_callback(
            '@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.-]*(\?\S+)?)?)?)@',
            function($matches){
                return
                "<a href=\"{$matches[1]}\" rel=\"nofollow\" target=\"_blank\">".parse_url($matches[1], PHP_URL_HOST)."</a>";
        }, $comment);

    }
}

if (! function_exists('jwtCheck')) {
    /**
     * Construir links a traves del string
     */
    function jwtCheck() {

        try {
            $user = Auth::parseToken()->authenticate();
            return $user ? true : false;
        } catch(\Tymon\JWTAuth\Exceptions\JWTException $e){
        }
    }
}