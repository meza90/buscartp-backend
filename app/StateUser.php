<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class StateUser extends Model
{
    protected $table = 'state_user';

    protected $fillable = ['state_id',  'user_id'];

    protected $appends = ['created_human'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function state()
    {
    	return $this->belongsTo(State::class)
                    ->select('model_id', 'method', 'state', 'id', 'created_at');
    }

    public function getCreatedHumanAttribute()
    {
        return Date::parse($this->created_at)->diffForHumans();
    }
}
