<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChannelTp extends Model
{
	protected $table = 'channel_tp';

	protected $hidden = ['created_at'];

    public function tp()
    {
    	return $this->belongsTo(Tp::class);
    }

    public function channel()
    {
    	return $this->belongsTo(Channel::class);
    }
}
