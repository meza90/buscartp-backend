<?php

namespace App\Mail;

use App\State;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificationState extends Mailable
{
    use Queueable, SerializesModels;

    public $state;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(State $state)
    {
        //
        $this->state = $state;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.state')
                    ->from('cristiansnake1990@gmail.com', 'Admin BuscarTP')
                    ->subject('Nuevo estado subido');
    }
}
