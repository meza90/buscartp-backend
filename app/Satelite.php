<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Satelite extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    public function scopeName($q, $name)
    {
    	return $q->where('name', 'LIKE', "%".str_replace('-', " ", $name)."%");
    }
}
