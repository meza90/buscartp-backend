<?php

namespace App;

use App\Notifications\StateNotify;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class State extends Model
{
    protected $fillable = ['model_id', 'state', 'update', 'method',
                            'user_id', 'ip', 'comment', 'country_id'];

    protected $appends = ['human_at', 'created_human'];

    protected $hidden = ['created_at', 'updated_at', 'ip'];

    //Eventos
    public static function boot()
    {
        parent::boot();

        static::created(function($state)
        {
            $state->notifications();

            if(jwtCheck()) $state->suscriptions()->create(array('user_id' => $state->user_id));
        });
    }

    //Relaciones
    public function satelites()
    {
        return $this->belongsToMany(Satelite::class, 'satelite_state')->withTimestamps();
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class, 'model_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    //Scopes
    public function scopeRelation($q)
    {
        return $q->with(
            'type:id,name,mark_id',
            'type.mark:id,name',
            'user:id,name,country_id',
            'user.country:id,name',
            'country:id,name'
        );
    }

    public function scopeSats($q)
    {
        return $q->with(['satelites' => function($q) { $q->orderBy('name', 'ASC'); }]);
    }

    public function scopeGetOf($q, $field, $request)
    {
        return $q->when($request, function($q) use($field, $request){
                    $q->where($field, $request);
                });
    }

    //Mutator
    public function setCommentAttribute($comment)
    {
        $this->attributes['comment'] = links($comment);
    }

    //Accessor
    public function getHumanAtAttribute()
    {
        return $this->created_at->format('d/m/Y h:i a');
    }

    public function getCreatedHumanAttribute()
    {
        return Date::parse($this->created_at)->diffForHumans();
    }

    //Otros
    public function notifications()
    {
        $this->type->suscriptions
            ->each(function($user){
                $user->user->notify(new StateNotify($this, $user->user->name));
            });
    }

    public function suscriptions()
    {
        return $this->hasMany(StateUser::class);
    }
}
