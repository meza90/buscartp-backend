<?php

namespace App;

use App\Notifications\StateCommentNotification;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Comment extends Model
{
    protected $fillable = ['state_id', 'body', 'user_id', 'ip'];

    protected $hidden = ['user_id', 'ip'];

    protected $appends = ['human_at', 'created_human_at'];

    public static function boot()
    {
        parent::boot();

        static::creating(function($comment)
        {
            $comment->suscriptions();
        });
    }

	public function state()
	{
		return $this->belongsTo(State::class);
	}

    public function getHumanAtAttribute()
    {
        return $this->created_at->format('d/m/Y h:i a');
    }

    public function setBodyAttribute($body)
    {
    	$this->attributes['body'] = links($body);
    }

    public function getCreatedHumanAtAttribute()
    {
        return Date::parse($this->created_at)->diffForHumans();
    }

    public function suscriptions()
    {
        $this->state->suscriptions
            ->each(function($user){
                $user->user->notify(new StateCommentNotification(
                    $this->state, $this->body, $user->user->name));
            });
    }
}
