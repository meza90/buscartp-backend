<?php

namespace App;

use App\Notifications\StateNotify;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;

class ModelUser extends Model
{
    protected $table = 'model_user';

    protected $fillable = ['model_id', 'user_id'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function type()
    {
    	return $this->belongsTo(Type::class);
    }
}
