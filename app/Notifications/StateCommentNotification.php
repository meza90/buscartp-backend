<?php

namespace App\Notifications;

use App\Comment;
use App\State;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class StateCommentNotification extends Notification implements ShouldQueue
{
    public $state;
    public $body;
    public $name;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(State $state, $body, $name)
    {
        $this->body = $body;
        $this->name = $name;
        $this->state = $state;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url("http://www.buscartp.com/states/{$this->state->id}/comments");
        $model = "{$this->state->type->mark->name} {$this->state->type->name}";

        return (new MailMessage)
                    ->view('emails.states.notifications', [
                        'url' => $url,
                        'name' => $this->name,
                        'model' => $model,
                        'body' => $this->body,
                    ])
                    ->subject("Nuevo comentario en estado {$model}")
                    ->from('buscartp.notifications@gmail.com', 'BuscarTP');
    }
}
