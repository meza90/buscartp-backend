<?php

namespace App\Notifications;

use App\State;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class StateNotify extends Notification implements ShouldQueue
{
    public $state;
    public $name;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(State $state, $name)
    {
        //
        $this->state = $state;
        $this->name = $name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url("http://www.buscartp.com/states/{$this->state->id}/comments");
        $model = "{$this->state->type->mark->name} {$this->state->type->name}";
        $state = $this->state->state;

        return (new MailMessage)
                    ->view('emails.models.notifications',
                    [
                        'name' => $this->name,
                        'state' => $this->state,
                        'url' => $url,
                        'model' => $model,
                        'status' => $state
                    ])
                    ->subject("Nuevo estado subido para {$model}")
                    ->from('buscartp.notifications@gmail.com', 'BuscarTP');
    }
}
