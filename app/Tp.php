<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tp extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'status', 'satelite_id'];

    public function satelite()
    {
    	return $this->belongsTo(Satelite::class);
    }

    public function channels()
    {
        return $this->belongsToMany(Channel::class)
                            ->orderBy('name', 'ASC')
                            ->withPivot('status', 'fta')
                            ->withTimestamps();
    }

    public function scopeFilter($q, $id)
    {
        $q->when($id, function($q) use ($id){
            return $q->where('id', $id);
        });
    }

    public function scopeGetSat($q, $satelite = 2)
    {
        $q->when($satelite, function($q) use ($satelite){
            return $q->where('satelite_id', $satelite);
        });
    }

    public function scopeConax($q)
    {
        return $q->selectRaw("REPLACE(name, ' Conax', '') as name, id");
    }

    public function scopeOrderAsc($q)
    {
        return $q->orderBy('name');
    }
}
