<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
	protected $hidden = ['created_at', 'updated_at'];

    public function toArray()
    {
        $attributes = $this->attributesToArray();
        $attributes = array_merge($attributes, $this->relationsToArray());
        unset($attributes['pivot']['created_at']);
        unset($attributes['pivot']['updated_at']);
        unset($attributes['pivot']['tp_id']);
        unset($attributes['pivot']['channel_id']);
        return $attributes;
    }
}
