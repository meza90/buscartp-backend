<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeUser extends Model
{
    protected $table = 'type_user';

    protected $fillable = ['type_id',  'user_id'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function type()
    {
    	return $this->belongsTo(Type::class);
    }
}
