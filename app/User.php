<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'country_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'role_id'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function suscriptions()
    {
        return $this->belongsToMany(TypeUser::class, 'type_user', 'user_id', 'type_id')
                        ->withTimestamps();
    }

    public function states()
    {
        return $this->hasMany(State::class)
                ->orderByDesc('created_at');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)
                ->orderByDesc('created_at');
    }

    public function subscribed()
    {
        return $this->hasMany(StateUser::class);
    }

    public function setPasswordAttribute($password)
    {
        if(!empty($password))
            $this->attributes['password'] = \Hash::make($password);
    }
}
