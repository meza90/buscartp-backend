<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
	protected $table = 'models';

    public function mark()
    {
    	return $this->belongsTo(Mark::class);
    }

    public function suscriptions()
    {
    	return $this->hasMany(TypeUser::class);
    }
}
